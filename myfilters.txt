[Adblock Plus 2.0]
! Version: 2
! Title: Josh's Custom List
! Last modified: 7 November 2018 19:10 PST
! Expires: 1 day (update frequency)
! Homepage: https://gitlab.com/jerquiaga/adblock-custom-list
!

! Newsblur.com 
newsblur.com##.NB-module.NB-module-river
newsblur.com##.NB-module-recommended.NB-module
newsblur.com##.NB-splash-module-section

! Cheezburger.com
cheezburger.com##iframe[style="margin: 0px; padding: 0px; border: none; width: 300px; height: 250px; float: left;"]
cheezburger.com##div[style="width: 100%; position: absolute; color: black; line-height: 11pt; font-size: 11px; text-align: left; bottom: 0px; left: 4px;"]

! Slashdot.org
slashdot.org##.row.deals-footer
slashdot.org###announcement
slashdot.org##.scw-wrap
slashdot.org##.scw-all-deals
slashdot.org##.p.ntv-body
slashdot.org##.nel-image

! Sanluisobispo.com
sanluisobispo.com##.agg-collage-content

! Taboola
taboola.com

! Simpli.fi
simpli.fi

! Outbrain
outbrain.com

! Coin-Hive
coin-hive.com

! Fool
fool.com

! ZergNet
zergnet.com

! Dianomi
dianomi.com

! Polygon Ads
polygon.com###google_ads_iframe_

! NYT
https://www.nytimes.com/vi-assets/static-assets/main*